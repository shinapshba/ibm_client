package shinapshba.ibm_client.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;

@SuppressWarnings({"unused", "deprecation"})
public class Category {

    private String language;
    private String retrieved_url;
    private Usage usage;
    ArrayList<Entry> categories = new ArrayList<Entry>();

    private static class Entry {
        private String label;
        private float score;

        public float getScore() {
            return score;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public void setScore(float score) {
            this.score = score;
        }

        @Override
        public String toString() {
            return String.format("label = %s, score = %s", label, score);
        }
    }

    private static class Usage {
        private float features;
        private float text_characters;
        private float text_units;

        public float getFeatures() {
            return features;
        }

        public float getText_characters() {
            return text_characters;
        }

        public float getText_units() {
            return text_units;
        }

        public void setFeatures(float features) {
            this.features = features;
        }

        public void setText_characters(float text_characters) {
            this.text_characters = text_characters;
        }

        public void setText_units(float text_units) {
            this.text_units = text_units;
        }
    }

    public String getLanguage() {
        return language;
    }

    public String getRetrieved_url() {
        return retrieved_url;
    }

    public Usage getUsage() {
        return usage;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setRetrieved_url(String retrieved_url) {
        this.retrieved_url = retrieved_url;
    }

    public void setUsage(Usage usageObject) {
        this.usage = usageObject;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(gson.toJson(this));
        return gson.toJson(je);
    }
}
