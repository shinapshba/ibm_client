package shinapshba.ibm_client.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;

@SuppressWarnings({"unused", "deprecation"})
public class Concept {
    private String language;
    private String retrieved_url;
    private Usage usage;
    ArrayList<Entry> concepts = new ArrayList<Entry>();

    private static class Entry {
        private String text;
        private float relevance;
        private String dbpedia_resource;

        public float getRelevance() {
            return relevance;
        }

        public String getText() {
            return text;
        }

        public String getDbpedia_resource() {
            return dbpedia_resource;
        }

        public void setText(String text) {
            this.text = text;
        }

        public void setRelevance(float relevance) {
            this.relevance = relevance;
        }

        public void setDbpedia_resource(String dbpedia_resource) {
            this.dbpedia_resource = dbpedia_resource;
        }

        @Override
        public String toString() {
            return String.format("text = %s, relevance = %s, dbpedia_resource = %s", text, relevance, dbpedia_resource);
        }
    }

    private static class Usage {
        private float features;
        private float text_characters;
        private float text_units;

        public float getFeatures() {
            return features;
        }

        public float getText_characters() {
            return text_characters;
        }

        public float getText_units() {
            return text_units;
        }

        public void setFeatures(float features) {
            this.features = features;
        }

        public void setText_characters(float text_characters) {
            this.text_characters = text_characters;
        }

        public void setText_units(float text_units) {
            this.text_units = text_units;
        }
    }

    public String getLanguage() {
        return language;
    }

    public String getRetrieved_url() {
        return retrieved_url;
    }

    public Usage getUsage() {
        return usage;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setRetrieved_url(String retrieved_url) {
        this.retrieved_url = retrieved_url;
    }

    public void setUsage(Usage usageObject) {
        this.usage = usageObject;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(gson.toJson(this));
        return gson.toJson(je);
    }
}
