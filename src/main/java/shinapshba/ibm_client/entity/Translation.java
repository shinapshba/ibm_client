package shinapshba.ibm_client.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;

@SuppressWarnings({"unused", "deprecation"})
public class Translation {

    private int word_count;
    private int character_count;
    private ArrayList<Entry> translations;

    private static class Entry{
        private String translation;

        public String getTranslation() {
            return translation;
        }

        public void setTranslation(String translation) {
            this.translation = translation;
        }
    }

    public ArrayList<Entry> getTranslations() {
        return translations;
    }

    public int getCharacter_count() {
        return character_count;
    }

    public int getWord_count() {
        return word_count;
    }

    public void setCharacter_count(int character_count) {
        this.character_count = character_count;
    }

    public void setTranslations(ArrayList<Entry> translations) {
        this.translations = translations;
    }

    public void setWord_count(int word_count) {
        this.word_count = word_count;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(gson.toJson(this));
        return gson.toJson(je);
    }
}
