package shinapshba.ibm_client.entity;

import com.google.gson.*;

import java.util.ArrayList;

@SuppressWarnings({"unused", "deprecation"})
public class Image {

    private int custom_classes;
    private int images_processed;
    private ArrayList<ImageEntry> images;

    private static class ClassEntry{
        private String class_;
        private double score;
        private String type_hierarchy;

        public double getScore() {
            return score;
        }

        public String getClass_() {
            return class_;
        }

        public String getType_hierarchy() {
            return type_hierarchy;
        }

        public void setScore(double score) {
            this.score = score;
        }

        public void setClass_(String class_) {
            this.class_ = class_;
        }

        public void setType_hierarchy(String type_hierarchy) {
            this.type_hierarchy = type_hierarchy;
        }
    }

    private static class ClassifierEntry{
        private String name;
        private String classifier_id;
        private ArrayList<ClassEntry> classes;

        public String getClassifier_id() {
            return classifier_id;
        }

        public String getName() {
            return name;
        }

        public ArrayList<ClassEntry> getClasses() {
            return classes;
        }

        public void setClassifier_id(String classifier_id) {
            this.classifier_id = classifier_id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setClasses(ArrayList<ClassEntry> classes) {
            this.classes = classes;
        }
    }

    private static class ImageEntry{
        private String source_url;
        private String resolved_url;
        private ArrayList<ClassifierEntry> classifiers;

        public ArrayList<ClassifierEntry> getClassifiers() {
            return classifiers;
        }

        public String getResolved_url() {
            return resolved_url;
        }

        public String getSource_url() {
            return source_url;
        }

        public void setClassifiers(ArrayList<ClassifierEntry> classifiers) {
            this.classifiers = classifiers;
        }

        public void setResolved_url(String resolved_url) {
            this.resolved_url = resolved_url;
        }

        public void setSource_url(String source_url) {
            this.source_url = source_url;
        }
    }

    public ArrayList<ImageEntry> getImages() {
        return images;
    }

    public int getCustom_classes() {
        return custom_classes;
    }

    public int getImages_processed() {
        return images_processed;
    }

    public void setCustom_classes(int custom_classes) {
        this.custom_classes = custom_classes;
    }

    public void setImages(ArrayList<ImageEntry> images) {
        this.images = images;
    }

    public void setImages_processed(int images_processed) {
        this.images_processed = images_processed;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(gson.toJson(this));
        return gson.toJson(je);
    }
}
