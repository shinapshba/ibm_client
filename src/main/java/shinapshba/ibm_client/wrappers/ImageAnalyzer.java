package shinapshba.ibm_client.wrappers;

import com.google.gson.Gson;
import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.visual_recognition.v3.VisualRecognition;
import com.ibm.watson.visual_recognition.v3.model.ClassifiedImages;
import com.ibm.watson.visual_recognition.v3.model.ClassifyOptions;
import shinapshba.ibm_client.entity.Image;

/**
 * Функционал анализа изображения
 */
@SuppressWarnings({"deprecation", "unused"})
public class ImageAnalyzer {

    private String apiKey;
    private String version;
    private String serviceUrl;
    private final VisualRecognition service;

    public ImageAnalyzer(String apiKey, String version, String serviceUrl) {
        this.apiKey = apiKey;
        this.version = version;
        this.serviceUrl = serviceUrl;
        Authenticator authenticator = new IamAuthenticator(apiKey);
        service = new VisualRecognition(version, authenticator);
        service.setServiceUrl(serviceUrl);
    }

    /**
     * Сбор данных об изображении
     *
     * @param url исходный URL адрес изображения
     * @return Объект Image
     */
    public Image getImageDesc(String url) {
        ClassifyOptions classifyOptions = new ClassifyOptions.Builder()
                .url(url)
                .build();
        ClassifiedImages result = service.classify(classifyOptions).execute().getResult();
        Gson gson = new Gson();
        return gson.fromJson(result.toString().replaceAll("\\bclass\\b", "class_"), Image.class);
    }

    //region Набор доступов
    public String getApiKey() {
        return apiKey;
    }

    public String getVersion() {
        return version;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }
    //endregion
}
