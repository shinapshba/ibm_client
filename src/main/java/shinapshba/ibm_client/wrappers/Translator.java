package shinapshba.ibm_client.wrappers;

import com.google.gson.Gson;
import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.language_translator.v3.LanguageTranslator;
import com.ibm.watson.language_translator.v3.model.Languages;
import com.ibm.watson.language_translator.v3.model.TranslateOptions;
import com.ibm.watson.language_translator.v3.model.TranslationResult;
import shinapshba.ibm_client.entity.Translation;

/**
 * Функционал перевода текста
 */
@SuppressWarnings({"unused"})
public class Translator {
    private String apiKey;
    private String version;
    private String serviceUrl;
    private final LanguageTranslator service;

    public Translator(String apiKey, String version, String serviceUrl) {
        this.apiKey = apiKey;
        this.version = version;
        this.serviceUrl = serviceUrl;
        Authenticator authenticator = new IamAuthenticator(apiKey);
        service = new LanguageTranslator(version, authenticator);
        service.setServiceUrl(serviceUrl);
    }

    /**
     * Перевод текста
     *
     * @param text   исходный текст
     * @param source код языка исходного текста
     * @param target код языка целевого текста
     * @return Объект Translation
     */
    public Translation translate(String text, String source, String target) {
        TranslateOptions translateOptions = new TranslateOptions.Builder()
                .addText(text)
                .modelId(String.format("%s-%s", source, target))
                .build();
        TranslationResult result = service.translate(translateOptions)
                .execute().getResult();

        Gson gson = new Gson();
        return gson.fromJson(result.toString(), Translation.class);
    }

    /**
     * Вывод поддерживаемых языков
     */
    public void listLanguages() {
        Languages languages = service.listLanguages()
                .execute().getResult();
        System.out.println(languages);
    }

    //region Набор доступов
    public String getServiceUrl() {
        return serviceUrl;
    }

    public String getVersion() {
        return version;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
    //endregion
}
