package shinapshba.ibm_client.wrappers;

import com.google.gson.Gson;
import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.natural_language_understanding.v1.NaturalLanguageUnderstanding;
import com.ibm.watson.natural_language_understanding.v1.model.*;
import shinapshba.ibm_client.entity.Category;
import shinapshba.ibm_client.entity.Concept;

/**
 * Функционал анализа текста web страниц
 */
@SuppressWarnings("unused")
public class TextAnalyzer {

    private String apiKey;
    private String version;
    private String serviceUrl;
    private final NaturalLanguageUnderstanding service;

    public TextAnalyzer(String apiKey, String version, String serviceUrl) {
        this.apiKey = apiKey;
        this.version = version;
        this.serviceUrl = serviceUrl;
        Authenticator authenticator = new IamAuthenticator(apiKey);
        service = new NaturalLanguageUnderstanding(version, authenticator);
        service.setServiceUrl(serviceUrl);
    }

    /**
     * Сбор данных о категории страницы
     *
     * @param url исходный URL адрес страницы
     * @return Объект Category
     */
    public Category getCategories(String url) {
        CategoriesOptions categories = new CategoriesOptions.Builder()
                .limit(3)
                .build();

        Features features = new Features.Builder()
                .categories(categories)
                .build();

        AnalyzeOptions parameters = new AnalyzeOptions.Builder()
                .url(url)
                .features(features)
                .build();

        AnalysisResults response = service
                .analyze(parameters)
                .execute()
                .getResult();

        Gson gson = new Gson();
        return gson.fromJson(response.toString(), Category.class);
    }

    /**
     * Сбор данных о концепте страницы
     *
     * @param url исходный URL адрес страницы
     * @return объект Concept
     */
    public Concept getConcept(String url) {
        ConceptsOptions concepts = new ConceptsOptions.Builder()
                .limit(3)
                .build();

        Features features = new Features.Builder()
                .concepts(concepts)
                .build();

        AnalyzeOptions parameters = new AnalyzeOptions.Builder()
                .url(url)
                .features(features)
                .build();

        AnalysisResults response = service
                .analyze(parameters)
                .execute()
                .getResult();

        Gson gson = new Gson();
        return gson.fromJson(response.toString(), Concept.class);
    }

    //region Набор доступов
    public String getApiKey() {
        return apiKey;
    }

    public String getVersion() {
        return version;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }
    //endregion
}
